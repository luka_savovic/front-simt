import Vue from 'vue'
import App from './App.vue'
import VueRouter from 'vue-router'
import router from './router'
import axios from 'axios'
import VueAxios from 'vue-axios'
import store from "@/store";  
import VModal from 'vue-js-modal'
import Datepicker from 'vuejs-datepicker';
import VueTimepicker from 'vue2-timepicker/src/vue-timepicker.vue'

import JsonExcel from "vue-json-excel";
 
Vue.component("downloadExcel", JsonExcel);



Vue.use(VueRouter)
Vue.use(VueRouter)
Vue.use(VModal, {adaptive:true})
Vue.config.productionTip = false


new Vue({
  router,
  VueAxios, 
  Datepicker,
  VueTimepicker,
  axios,
  store: store,
  render: h => h(App)
}).$mount('#app')
