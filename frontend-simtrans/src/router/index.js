import Vue from 'vue'
import VueRouter from 'vue-router'
import Home from '../views/Home.vue'
import Tasks from '../views/Tasks.vue'
import AdminBoard from '../views/AdminBoard.vue'
import Orders from '../views/Orders.vue'
import Drivers from '../views/Drivers.vue'
import Clients from '../views/Clients.vue'
import Trucks from '../views/Trucks.vue'
import UserEamils from '../views/UserEmails.vue'
import DriverProfile from '../views/DriverProfile.vue'
import Todo from '../views/Todo.vue'
import Perdiem from '../views/Perdiem.vue'
import Invoice from '../views/Invoice.vue'
import ClientProfile from '../views/ClientProfile'
import ArtPetrol from '../views/ArtPetrol'
import SuppliersProfile from '../views/SupplierProfile.vue'
import Suppliers from '../views/Suppliers.vue'
import TruckProfile from '../views/TrucksProfile.vue'
import Trailers from '../views/Trailers.vue'
import TrailerProfile from '../views/TrailerProfile.vue'
import Containers from '../views/Containers.vue'
import ContainerProfile from '../views/ContainerProfile.vue'
import Login from '../views/Login.vue'
// import Dnevnice from '../views/Dnevnice.vue'

Vue.use(VueRouter)



const routes = [
  {
    path: '/',
    name: 'Home',
    component: Home
  },
  {
    path: '/tasks',
    name: 'Tasks',
    component: Tasks
  },
  {
    path: '/adminboard',
    name: 'AdminBoard',
    component: AdminBoard
  },
  {
    path: '/orders',
    name: 'Orders',
    component: Orders
  },
  {
    path: '/drivers',
    name: 'Drivers',
    component: Drivers,
  },
  {
    path: '/clients',
    name: 'Clients',
    component: Clients
  },
  {
    path: '/clients/:id',
    name: 'ClientProfile',
    component: ClientProfile
  },
  {
    path: '/suppliers',
    name: 'Suppliers',
    component: Suppliers
  },
  {
    path: '/suppliers/:id',
    name: 'SuppliersProfile',
    component: SuppliersProfile
  },
  {
    path: '/trucks',
    name: 'Trucks',
    component: Trucks
  },
  {
    path: '/trailers',
    name: 'Trailers',
    component: Trailers
  },
  {
    path: '/trucks/:id',
    name: 'TrucksProfile',
    component: TruckProfile
  },
  {
    path: '/containers',
    name: 'Containers',
    component: Containers
  },
  {
    path: '/containers/:id',
    name: 'ContainerProfile',
    component: ContainerProfile
  },
  {
    path: '/trailers/:id',
    name: 'TrailerProfile',
    component: TrailerProfile
  },
  {
    path: '/userEmails',
    name: 'UserEamils',
    component: UserEamils
  },
  {
    path: '/drivers/:id',
    name: 'DriverProfile',
    component: DriverProfile
  },
  {
    path: '/perdiem/:id',
    name: 'Perdiem',
    component: Perdiem
  },
  {
    path: '/todo',
    name: 'Todo',
    component: Todo  
  },
  {
    path: '/invoice',
    name: 'Invoice',
    component: Invoice  
  },
  {
    path: '/artpetrol',
    name: 'Artpetrol',
    component: ArtPetrol  
  },
  {
    path: '/login',
    name: 'Login',
    component: Login,
    beforeEnter:((to,from,next)=>{
      if(localStorage.getItem("jwt")){
        next({
          path: '',
          replace: true
        })
      } else{
        next();
      }
    }),     
  }
]

const router = new VueRouter({
  mode: 'history',
  base: process.env.BASE_URL,
  routes
})


router.beforeEach((to, from, next)=>{
  let token = localStorage.getItem("jwt");
  if ( to.name !== 'Login' && token == null ){
    next({
      path: 'login',
      replace: true
    })
  } else {
    next();
  }
})

export default router
