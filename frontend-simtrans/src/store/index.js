import Vue from "vue";
import Vuex from "vuex";

Vue.use(Vuex);

export default new Vuex.Store({
  state: {
    notification: []
  },

  getters: {

  },

  mutations: {
    PUSH_NOTIFICATION(state, notification) {
        state.notification.push({
            ...notification,
            id: (Math.random().toString(36) + Date.now().toString(36)).substr(2)
        })
    }
  },

  actions: {
    addNotification({commit}, notification) {
        commit('PUSH_NOTIFICATION', notification)
    }
  }
});